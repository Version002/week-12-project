<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

use App\Models\Todos;

class TodosController extends Controller
{
    //
    public function index() {

        $publicTodos = Todos::where('status', '=', 'public')->get();
        
        return response([
            'todos' => $publicTodos,
        ]);
    }

    public function get() {
        $todos = auth()->user()->todos()->get();

        return response([
            'todos' => $todos,
        ]);
    }

    public function create(Request $request) {
        $field = $request->validate([
            'title' => 'required|string',
            'description' => 'required|string|max:1000',
            'status' => 'required|in:public,private',
        ]);

        $field['user_id'] = Auth::id();

        $todos = Todos::create($field);

        return response([
            'todos' => $todos,
        ]);
    }

    public function edit(Request $request, $id) {
        $field = $request->validate([
            'title' => 'string',
            'description' => 'string|max:1000',
            'status' => 'in:public,private'
        ]);

        $todos = Todos::find($id);
        
        if($todos->user_id === Auth::id()) {
            $todos->update($field);

            return response([
                'todos' => $todos,
            ], 200);
        } else {
            return response([
                'message' => 'cannot edit other user\'s todo'
            ]);
        }

        
    }

    public function delete($id) {
        $todos = Todos::find($id);
        $todos->delete();

        return response([
            'message' => $todos
        ], 200);
    }
}
