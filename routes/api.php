<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TodosController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::get('/users', [AuthController::class, 'index']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/logout', [AuthController::class, 'logout']);
    
    Route::get('/todos', [TodosController::class, 'index']);
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/todos/user/', [TodosController::class, 'get']);
    Route::post('/todos/user/', [TodosController::class, 'create']);
    Route::put('/todos/user/{id}', [TodosController::class, 'edit']);
    Route::delete('/todos/user/{id}', [TodosController::class, 'delete']);
});

